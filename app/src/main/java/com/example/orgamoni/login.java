package com.example.orgamoni;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class login extends Fragment {
   EditText phone_number;
   Button login;
   String verificationId;
   ProgressBar progressBar;
   FirebaseAuth auth;
   TextView message;
    public login() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_login, container, false);
        auth=FirebaseAuth.getInstance();
        progressBar=(ProgressBar)view.findViewById(R.id.progressBar);
        phone_number=(EditText)view.findViewById(R.id.phone_number);
        message=(TextView) view.findViewById(R.id.message);
        login=(Button) view.findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phone_number.getText().toString().isEmpty()){
                    phone_number.setError("Number required");
                    phone_number.requestFocus();
                }
                else{
                    progressBar.setVisibility(View.VISIBLE);
                    message.setText("waiting to automically detect a message sent to "+phone_number.getText());
                    message.setVisibility(View.VISIBLE);
                   sendVerificationCode(phone_number.getText().toString());
                }
            }
        });
        return view;
    }
    public void sendVerificationCode(String number){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(number,90, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD,mCallBacks);
    }
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBacks=new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId=s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code=phoneAuthCredential.getSmsCode();
            if (code!=null){
                progressBar.setVisibility(View.VISIBLE);
                verify(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getActivity(),""+e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    };
    public void verify(String code) {

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        auth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    message.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    FirebaseDatabase.getInstance().getReference().child("user_details").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                business b = snapshot.getValue(business.class);
                                if (b.getOwner_number().equals(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())) {
                                    Intent i = new Intent(getActivity(), Home.class);
                                    startActivity(i);
                                }
                            }
                            business_details details = new business_details();
                            getFragmentManager().beginTransaction().replace(R.id.container, details).addToBackStack("business").commit();

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                } else {
                    message.setVisibility(View.INVISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getActivity(), "" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                message.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
