package com.example.orgamoni;

import java.util.ArrayList;

public class business {

    String name;
    String owner_number;
    ArrayList<String> product_services;

    public business(String name, String owner_number, ArrayList<String> product_services) {
        this.name = name;
        this.owner_number = owner_number;
        this.product_services = product_services;
    }

    public business() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_number() {
        return owner_number;
    }

    public void setOwner_number(String owner_number) {
        this.owner_number = owner_number;
    }

    public ArrayList<String> getProduct_services() {
        return product_services;
    }

    public void setProduct_services(ArrayList<String> product_services) {
        this.product_services = product_services;
    }
}
