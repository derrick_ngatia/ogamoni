package com.example.orgamoni;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class business_details extends Fragment {

    Button skip,create_account;
    CheckBox terms;
    FirebaseAuth auth;
    FirebaseDatabase database;
    DatabaseReference user_details;
    EditText business_name,product_services;
    ProgressBar bar;
    public business_details() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view=inflater.inflate(R.layout.fragment_business_details, container, false);
        bar=(ProgressBar) view.findViewById(R.id.progressBar);
        auth=FirebaseAuth.getInstance();
        database=FirebaseDatabase.getInstance();
        user_details=database.getReference().child("user_details");
        skip=(Button) view.findViewById(R.id.skip);
        terms=(CheckBox) view.findViewById(R.id.terms);
        create_account=(Button) view.findViewById(R.id.create_account);
        business_name=(EditText)  view.findViewById(R.id.business_name);
        product_services=(EditText) view.findViewById(R.id.product_services);
        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!terms.isChecked()){
                    terms.setError("please accept terms and conditions to continue");
                    terms.requestFocus();
                }else if(business_name.getText().toString().isEmpty()){
                    business_name.setError("Business name is required");
                    business_name.requestFocus();
                }
                else if(product_services.getText().toString().isEmpty()){
                    product_services.setError("product or service is required");
                    product_services.requestFocus();
                }
                else {
                    bar.setVisibility(View.VISIBLE);
                    ArrayList<String> s=new ArrayList<>();
                    s.add(product_services.getText().toString());

                    business b =new business(business_name.getText().toString(),auth.getCurrentUser().getPhoneNumber(),s);
                    user_details.push().setValue(b).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()){
                                Toast.makeText(getActivity(),"Account details added",Toast.LENGTH_SHORT).show();
                                Intent i=new Intent(view.getContext(),Home.class);
                                startActivity(i);
                                bar.setVisibility(View.INVISIBLE);
                            }else{
                                bar.setVisibility(View.INVISIBLE);
                                Toast.makeText(getActivity(),""+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getActivity(),""+e.getMessage(),Toast.LENGTH_SHORT).show();
                            bar.setVisibility(View.INVISIBLE);
                        }
                    });


                }
            }
        });
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(v.getContext(),Home.class);
                startActivity(i);
            }
        });
        return view;
    }

}
