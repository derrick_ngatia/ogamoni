package com.example.orgamoni;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class register extends Fragment {
    Spinner spinner;
    EditText phone_number;
    Button register;

    public register() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_register, container, false);
        spinner=(Spinner)view.findViewById(R.id.spinner);
        register=(Button) view.findViewById(R.id.register);
        phone_number=(EditText) view.findViewById(R.id.phone_number);
        final ArrayAdapter<String> adapter=new ArrayAdapter<>(view.getContext(),R.layout.support_simple_spinner_dropdown_item,codes.codes);
        spinner.setAdapter(adapter);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phone_number.getText().toString().isEmpty()){
                    phone_number.setError("Number required");
                    phone_number.requestFocus();
                }
                else{
                    String code=codes.codes[spinner.getSelectedItemPosition()];
                    String number=phone_number.getText().toString();
                    String phone_number=code+number;
                    Bundle bundle=new Bundle();
                    bundle.putString("phone",phone_number);
                    verify verify=new verify();
                    verify.setArguments(bundle);
                    getFragmentManager().beginTransaction().replace(R.id.container,verify).addToBackStack("verify").commit();
                }
            }
        });

        return  view;
    }

}
