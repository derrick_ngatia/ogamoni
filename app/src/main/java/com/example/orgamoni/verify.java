package com.example.orgamoni;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class verify extends Fragment {
   Button verify;
   EditText code;
   String verificationId;
   FirebaseAuth auth;
   ProgressBar progressBar;

    public verify() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       final View view=inflater.inflate(R.layout.fragment_verify, container, false);
        auth=FirebaseAuth.getInstance();
        progressBar=(ProgressBar) view.findViewById(R.id.progressBar);
        verify=(Button) view.findViewById(R.id.verify);
        code=(EditText) view.findViewById(R.id.code);
        String phone=getArguments().getString("phone");
        sendVerificationCode(phone);
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             progressBar.setVisibility(View.VISIBLE);
             if (code.getText().toString().isEmpty()){
                 code.setError("code required");
                 code.requestFocus();
             }else{
                 verify(code.getText().toString());
             }
            }
        });
        return  view;
    }
    public void sendVerificationCode(String number){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(number,90, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD,mCallBacks);
    }
    public void verify(String code){

        PhoneAuthCredential credential=PhoneAuthProvider.getCredential(verificationId,code);
        auth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    progressBar.setVisibility(View.INVISIBLE);
                    FirebaseDatabase.getInstance().getReference().child("user_details").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            for (DataSnapshot snapshot:dataSnapshot.getChildren()) {
                                business b=snapshot.getValue(business.class);
                                if(b.getOwner_number().equals(FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber())){
                                    Toast.makeText(getActivity(),"Already have an account",Toast.LENGTH_SHORT).show();
                                    Intent i=new Intent(getActivity(),Home.class);
                                    startActivity(i);
                                }
                            }
                            business_details details=new business_details();
                            getFragmentManager().beginTransaction().replace(R.id.container,details).addToBackStack("business").commit();

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
                else{
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(getActivity(),""+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(),""+e.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });

    }
   private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallBacks=new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
       @Override
       public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
           super.onCodeSent(s, forceResendingToken);
           verificationId=s;
       }

       @Override
       public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
        String code=phoneAuthCredential.getSmsCode();
        if (code!=null){
            progressBar.setVisibility(View.VISIBLE);
            verify(code);
        }
       }

       @Override
       public void onVerificationFailed(FirebaseException e) {
           Toast.makeText(getActivity(),""+e.getMessage(),Toast.LENGTH_SHORT).show();
       }
   };


}
