package com.example.orgamoni;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class launcher extends Fragment {
    Button new_user,existing_user;


    public launcher() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_launcher, container, false);
        new_user=(Button)view.findViewById(R.id.new_user);
        existing_user=(Button)view.findViewById(R.id.existing_user);
        existing_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login login=new login();
                getFragmentManager().beginTransaction().replace(R.id.container,login).addToBackStack("login").commit();
            }
        });
        new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register register=new register();
                getFragmentManager().beginTransaction().replace(R.id.container,register).addToBackStack("register").commit();
            }
        });


        return view;
    }

}
