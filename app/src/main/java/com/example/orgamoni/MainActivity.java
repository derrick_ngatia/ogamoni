package com.example.orgamoni;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        launcher launcher=new launcher();
        getSupportFragmentManager().beginTransaction().replace(R.id.container,launcher).commit();
    }
}
